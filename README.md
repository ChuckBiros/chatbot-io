# Chatbots IO

Étienne MORVAN, Alexis LEFOUL, Julien COULON, Boris MONTRON, Dorian LONGÉPÉE

Chatbots IO est une application de chat utilisant plusieurs bots pour fournir différentes réponses et actions en fonction des commandes utilisateur.

## Fonctionnalités

- Sélection de différents bots avec des personnalités et des réponses uniques.
- Envoi de messages par l'utilisateur et réponses des bots.
- Sauvegarde de l'historique des conversations dans le `localStorage`.
- Affichage de la date et de l'heure des messages.
- Envoi de messages en appuyant sur la touche "Entrée".

## Installation

1. Clonez le dépôt :
    git clone https://gitlab.com/ChuckBiros/chatbot-io.git
    cd chatbot-IO
    

2. Installez les dépendances :
    npm install
    

3. Démarrez l'application :
    npm start
    

## Utilisation

1. Ouvrez votre navigateur et allez à `http://127.0.0.1:9090/`.
2. Sélectionnez un bot dans la liste à gauche.
3. Tapez un message dans le commande de saisie en bas et appuyez sur "Entrée" ou cliquez sur le bouton "Envoyer".
4. Les messages seront affichés dans la zone de chat à droite avec la date et l'heure d'envoi.

## Structure du projet

- `index.scss` : Fichier de style principal.
- `bot/bot.js` : Définition de la classe Bot.
- `services/api.js` : Service pour interagir avec l'API.
- `index.js` : Fichier principal contenant la logique de l'application et l'initialisation des bots.